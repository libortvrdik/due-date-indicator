/*
Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/

package com.stygian.jira.plugins.customfields;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.util.JiraDateUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.issue.customfields.impl.TextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class OverDueCustomField extends GenericTextCFType {
    private static final Logger log = LoggerFactory.getLogger(OverDueCustomField.class);

    public OverDueCustomField(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager) {
        super(customFieldValuePersister, genericConfigManager);
    }

    @Override
    public Map<String, Object> getVelocityParameters(final Issue issue,
                                                     final CustomField field,
                                                     final FieldLayoutItem fieldLayoutItem) {
        final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);

        // This method is also called to get the default value, in
        // which case issue is null so we can't use it to add currencyLocale
        if (issue == null) {
            return map;
        }

        DateTimeFormatterFactory dateTimeFormatterFactory = ComponentAccessor.getComponent(DateTimeFormatterFactory.class);
        DefaultIssueChangeHolder issueChangeHolder = new DefaultIssueChangeHolder();

        FieldConfig fieldConfig = field.getRelevantConfig(issue);
        if(issue.getResolutionObject() != null){
            map.put("resolution",issue.getResolutionDate());
            field.updateValue(fieldLayoutItem, issue, new ModifiedValue(field.getValue(issue), null), issueChangeHolder);
        }
        //add what you need to the map here
        if(issue.getResolutionObject() == null && issue.getDueDate() != null)
        {
            long diffInMillies = issue.getDueDate().getTime() - new Timestamp(new Date().getTime()).getTime();
            DateTime dueDate = new DateTime(issue.getDueDate());
            DateTime today = new DateTime();
            Integer days = Days.daysBetween(dueDate,today).getDays();
            map.put("days",days);
            field.updateValue(fieldLayoutItem, issue, new ModifiedValue(field.getValue(issue), days.toString()), issueChangeHolder);
        }

        field.store();

        return map;
    }
}